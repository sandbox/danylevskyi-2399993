
ABOUT THE MODULE
----------------

Panels Node UUID module provides configurable Page Manager pane that can be used
to hold a node loaded by UUID. That can be useful to export Page Manager pages
with Features module.

DEPENDENCIES
------------

 * Page Manager
 * UUID

HOW TO USE
----------

 1. Install the dependencies.
 2. Create page variant with Page Manager.
 3. Go to 'Content' vertical tab and click on 'Add content' link.
 4. Click on 'Existing node by UUID' item which sits under 'UUID' vertical tab.
 5. Provide UUID of a node and click 'Finish' button.
