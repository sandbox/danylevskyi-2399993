<?php

/**
 * @file
 * Plugin to handle the 'node' content type which allows individual nodes
 * to be embedded into a panel by UUID.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'single' => TRUE,
  'defaults' => array(
    'uuid' => '',
    'build_mode' => 'full',
    'link_node_title' => FALSE,
  ),
  'title' => t('Existing node by UUID'),
  'description' => t('Add a node from your site as content.'),
  'category' => t('UUID'),
);

/**
 * Render callback.
 */
function panels_node_uuid_node_uuid_content_type_render($subtype, $conf, $panel_args) {
  $block = new stdClass();

  if (!$node = panels_node_uuid_get_node($conf['uuid'])) {
    $block->content = t('Not found');
    return $block;
  }

  if (!node_access('view', $node)) {
    $block->content = t('Access denied');
    return $block;
  }

  $node = clone($node);

  $block->title = check_plain($node->title);
  if (!empty($conf['link_node_title'])) {
    $block->title_link = 'node/' . $node->nid;
  }

  $node->title = NULL;

  $block->content = node_view($node, $conf['build_mode']);
  $block->content['links']['#access'] = FALSE;

  return $block;
}

/**
 * Form constructor.
 */
function panels_node_uuid_node_uuid_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['uuid'] = array(
    '#title' => t('Enter the UUID of a node'),
    '#type' => 'textfield',
    '#default_value' => $conf['uuid'],
    '#required' => TRUE,
    '#weight' => -10,
  );

  $entity = entity_get_info('node');
  $build_mode_options = array();
  foreach ($entity['view modes'] as $mode => $option) {
    $build_mode_options[$mode] = $option['label'];
  }
  $form['build_mode'] = array(
    '#title' => t('Build mode'),
    '#type' => 'select',
    '#description' => t('Select a build mode for this node.'),
    '#options' => $build_mode_options,
    '#default_value' => $conf['build_mode'],
    '#weight' => -9,
  );

  $form['link_node_title'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($conf['link_node_title']),
    '#title' => t('Link the node title to the node'),
    '#description' => t('Check this box if you would like your pane title to link to the node.'),
  );

  return $form;
}

/**
 * Validate callback.
 */
function panels_node_uuid_node_uuid_content_type_edit_form_validate(&$form, &$form_state) {
  $uuid = $form_state['values']['uuid'];
  if (!$node = panels_node_uuid_get_node($uuid)) {
    form_error($form['uuid'], t('UUID not found.'));
  }
}

/**
 * Submit callback.
 */
function panels_node_uuid_node_uuid_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('uuid', 'build_mode', 'link_node_title') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Return the administrative title for a pane.
 */
function panels_node_uuid_node_uuid_content_type_admin_title($subtype, $conf) {
  $node = panels_node_uuid_get_node($conf['uuid']);
  if ($node) {
    if (!empty($node->status) || user_access('administer nodes')) {
      return t('Node by UUID: @uuid', array('@uuid' => $conf['uuid']));
    }
    else {
      return t('Unpublished node @uuid', array('@uuid' => $conf['uuid']));
    }
  }
  else {
    return t('Deleted/missing node @uuid', array('@uuid' => $conf['uuid']));
  }
}

/**
 * Return the administrative information for a pane.
 */
function panels_node_uuid_node_uuid_content_type_admin_info($subtype, $conf) {
  return panels_node_uuid_node_uuid_content_type_render($subtype, $conf, array());
}
